package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.Constants;
import com.github.gavlyukovskiy.bankapplication.JdbiTest;
import com.github.gavlyukovskiy.bankapplication.domain.Account;
import com.github.gavlyukovskiy.bankapplication.domain.Currency;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionStatus;
import com.github.gavlyukovskiy.bankapplication.domain.web.DepositRequest;
import com.github.gavlyukovskiy.bankapplication.domain.web.SubmitTransactionRequest;
import com.github.gavlyukovskiy.bankapplication.exception.HttpException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransactionServiceTest extends JdbiTest {

    @Mock
    private AccountService accountService;
    @Mock
    private TransactionEventBus transactionEventBus;

    private TransactionService transactionService;

    @BeforeEach
    void setUp() {
        transactionService = new TransactionService(jdbi, accountService, transactionEventBus);
    }

    @ParameterizedTest
    @EnumSource(Currency.class)
    void shouldCreateTransactionForProcessing(Currency currency) {
        Account senderAccount = createAccount("Test sender", 100.42, currency);
        Account receiverAccount = createAccount("Test receiver", 0, currency);
        when(accountService.getAccount(senderAccount.getAccountId())).thenReturn(Optional.of(senderAccount));
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setSenderId(senderAccount.getAccountId());
        submitTransactionRequest.setReceiverId(receiverAccount.getAccountId());
        submitTransactionRequest.setAmount(100.42);
        submitTransactionRequest.setDescription("Test transaction");
        var transaction = transactionService.submitTransaction(submitTransactionRequest, false);

        assertNotNull(transaction.getTransactionId());
        assertEquals(100.42, transaction.getAmount());
        assertEquals(senderAccount.getAccountId(), transaction.getSenderId());
        assertEquals(receiverAccount.getAccountId(), transaction.getReceiverId());
        assertEquals(TransactionStatus.SUBMITTED, transaction.getStatus());
        assertEquals("Test transaction", transaction.getDescription());
        var createdMillisAgo = ChronoUnit.MILLIS.between(transaction.getCreatedAt(), LocalDateTime.now());
        assertTrue(createdMillisAgo < 500, "transaction.createdAt should be within 500ms window, was " + createdMillisAgo + "ms");

        verify(transactionEventBus).transactionStatusChange(transaction);

        var retrievedTransaction = transactionService.getTransaction(transaction.getTransactionId());
        assertTrue(retrievedTransaction.isPresent());
        assertEquals(transaction.getTransactionId(), retrievedTransaction.get().getTransactionId());
        assertEquals(100.42, retrievedTransaction.get().getAmount());
        assertEquals(senderAccount.getAccountId(), retrievedTransaction.get().getSenderId());
        assertEquals(receiverAccount.getAccountId(), retrievedTransaction.get().getReceiverId());
        assertEquals(TransactionStatus.SUBMITTED, retrievedTransaction.get().getStatus());
        assertEquals("Test transaction", retrievedTransaction.get().getDescription());
    }

    @Test
    void shouldFailToTransferMoneyBetweenDifferentCurrencies() {
        Account senderAccount = createAccount("Test sender", 100.42, Currency.USD);
        Account receiverAccount = createAccount("Test receiver", 0, Currency.EUR);
        when(accountService.getAccount(senderAccount.getAccountId())).thenReturn(Optional.of(senderAccount));
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setSenderId(senderAccount.getAccountId());
        submitTransactionRequest.setReceiverId(receiverAccount.getAccountId());
        submitTransactionRequest.setAmount(100.42);
        submitTransactionRequest.setDescription("Test transaction");

        var httpException = assertThrows(HttpException.class, () -> transactionService.submitTransaction(submitTransactionRequest, false));
        assertEquals(400, httpException.statusCode());
        assertEquals("Transfer between accounts with different currencies is not supported", httpException.getMessage());

        verifyNoMoreInteractions(transactionEventBus);
    }

    @Test
    void shouldFailToTransferInvalidAmount() {
        Account senderAccount = createAccount("Test sender", 100, Currency.USD);
        Account receiverAccount = createAccount("Test receiver", 0, Currency.USD);

        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setSenderId(senderAccount.getAccountId());
        submitTransactionRequest.setReceiverId(receiverAccount.getAccountId());
        submitTransactionRequest.setAmount(42.123);
        submitTransactionRequest.setDescription("Test transaction");

        var httpException = assertThrows(HttpException.class, () -> transactionService.submitTransaction(submitTransactionRequest, false));
        assertEquals(400, httpException.statusCode());
        assertEquals("Invalid amount", httpException.getMessage());

        verifyNoMoreInteractions(transactionEventBus);
    }

    @Test
    void shouldFailToTransferFromInvalidAccount() {
        Account senderAccount = createAccount("Test sender", 100.42, Currency.USD);
        when(accountService.getAccount("00000000-0000-0000-0000-000000000001")).thenReturn(Optional.empty());

        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setSenderId(senderAccount.getAccountId());
        submitTransactionRequest.setReceiverId("00000000-0000-0000-0000-000000000001");
        submitTransactionRequest.setAmount(42.42);
        submitTransactionRequest.setDescription("Test transaction");

        var httpException = assertThrows(HttpException.class, () -> transactionService.submitTransaction(submitTransactionRequest, false));
        assertEquals(400, httpException.statusCode());
        assertEquals("Account [00000000-0000-0000-0000-000000000001] does not exists", httpException.getMessage());

        verifyNoMoreInteractions(transactionEventBus);
    }

    @Test
    void shouldFailToTransferToTheSameAccount() {
        Account senderAccount = createAccount("Test sender", 100.42, Currency.USD);
        when(accountService.getAccount(senderAccount.getAccountId())).thenReturn(Optional.of(senderAccount));

        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setSenderId(senderAccount.getAccountId());
        submitTransactionRequest.setReceiverId(senderAccount.getAccountId());
        submitTransactionRequest.setAmount(42.42);
        submitTransactionRequest.setDescription("Test transaction");

        var httpException = assertThrows(HttpException.class, () -> transactionService.submitTransaction(submitTransactionRequest, false));
        assertEquals(400, httpException.statusCode());
        assertEquals("Cannot transfer money to the same account", httpException.getMessage());

        verifyNoMoreInteractions(transactionEventBus);
    }

    @Test
    void shouldFailToTransferToInvalidAccount() {
        Account receiverAccount = createAccount("Test receiver", 0, Currency.USD);
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));
        when(accountService.getAccount("00000000-0000-0000-0000-000000000001")).thenReturn(Optional.empty());

        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setSenderId("00000000-0000-0000-0000-000000000001");
        submitTransactionRequest.setReceiverId(receiverAccount.getAccountId());
        submitTransactionRequest.setAmount(42.42);
        submitTransactionRequest.setDescription("Test transaction");

        var httpException = assertThrows(HttpException.class, () -> transactionService.submitTransaction(submitTransactionRequest, false));
        assertEquals(400, httpException.statusCode());
        assertEquals("Account [00000000-0000-0000-0000-000000000001] does not exists", httpException.getMessage());

        verifyNoMoreInteractions(transactionEventBus);
    }

    @Test
    void shouldFailToTransferWhenBalanceIsLessThanAmount() {
        Account senderAccount = createAccount("Test sender", 100, Currency.USD);
        Account receiverAccount = createAccount("Test receiver", 0, Currency.USD);
        when(accountService.getAccount(senderAccount.getAccountId())).thenReturn(Optional.of(senderAccount));
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setSenderId(senderAccount.getAccountId());
        submitTransactionRequest.setReceiverId(receiverAccount.getAccountId());
        submitTransactionRequest.setAmount(100.42);
        submitTransactionRequest.setDescription("Test transaction");

        var httpException = assertThrows(HttpException.class, () -> transactionService.submitTransaction(submitTransactionRequest, false));
        assertEquals(400, httpException.statusCode());
        assertEquals("Insufficient funds", httpException.getMessage());

        verifyNoMoreInteractions(transactionEventBus);
    }

    @Test
    void shouldFailToTransferIfDescriptionIsTooLong() {
        Account senderAccount = createAccount("Test sender", 100.42, Currency.USD);
        Account receiverAccount = createAccount("Test receiver", 0, Currency.USD);
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setSenderId(senderAccount.getAccountId());
        submitTransactionRequest.setReceiverId(receiverAccount.getAccountId());
        submitTransactionRequest.setAmount(100.42);
        submitTransactionRequest.setDescription("Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
                " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type an");

        var httpException = assertThrows(HttpException.class, () -> transactionService.submitTransaction(submitTransactionRequest, false));
        assertEquals(400, httpException.statusCode());
        assertEquals("Description must be within 200 symbols", httpException.getMessage());

        verifyNoMoreInteractions(transactionEventBus);
    }

    @Test
    void shouldUseServiceAccountToDepositToAccount() {
        Account receiverAccount = createAccount("Test receiver", 0, Currency.USD);
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        var accountDepositRequest = new DepositRequest();
        accountDepositRequest.setAccountId(receiverAccount.getAccountId());
        accountDepositRequest.setAmount(100.42);
        accountDepositRequest.setReason("Test deposit");
        var transaction = transactionService.makeDeposit(accountDepositRequest);

        assertNotNull(transaction.getTransactionId());
        assertEquals(100.42, transaction.getAmount());
        assertEquals(Constants.SERVICE_ACCOUNT_ID, transaction.getSenderId());
        assertEquals(receiverAccount.getAccountId(), transaction.getReceiverId());
        assertEquals(TransactionStatus.TRANSIT, transaction.getStatus());
        assertEquals("Internal deposit operation: Test deposit", transaction.getDescription());
        var createdMillisAgo = ChronoUnit.MILLIS.between(transaction.getCreatedAt(), LocalDateTime.now());
        assertTrue(createdMillisAgo < 500, "transaction.createdAt should be within 500ms window, was " + createdMillisAgo + "ms");

        verify(transactionEventBus).transactionStatusChange(transaction);
    }

    private Account createAccount(String accountName, double balance, Currency currency) {
        var senderAccount = new Account();
        senderAccount.setAccountId(UUID.randomUUID().toString());
        senderAccount.setAccountName(accountName);
        senderAccount.setBalance(balance);
        senderAccount.setCurrency(currency);
        return senderAccount;
    }
}
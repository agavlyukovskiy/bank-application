package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.JdbiTest;
import com.github.gavlyukovskiy.bankapplication.dao.AccountDao;
import com.github.gavlyukovskiy.bankapplication.dao.TransactionDao;
import com.github.gavlyukovskiy.bankapplication.domain.Account;
import com.github.gavlyukovskiy.bankapplication.domain.Currency;
import com.github.gavlyukovskiy.bankapplication.domain.Transaction;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionDenyReason;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransactionProcessingServiceTest extends JdbiTest {

    @Mock
    private AccountService accountService;
    @Mock
    private TransactionService transactionService;
    @Mock
    private AccountDao accountDao;
    @Mock
    private TransactionDao transactionDao;

    private TransactionEventBus transactionEventBus = new TransactionEventBus();

    private Properties properties;

    @BeforeEach
    void setUp() {
        properties = new Properties();
        try (var propertiesStream = TransactionProcessingServiceTest.class.getResourceAsStream("/application.properties")) {
            properties.load(propertiesStream);
        } catch (IOException e) {
            throw new UncheckedIOException("Failed to load application.properties", e);
        }
        addJdbiMockDao(AccountDao.class, accountDao);
        addJdbiMockDao(TransactionDao.class, transactionDao);
    }

    @ParameterizedTest
    @EnumSource(Currency.class)
    void shouldUseProcessorsToPerformTransactions(Currency currency) {
        TransactionProcessingService transactionProcessingService = new TransactionProcessingService(properties, jdbi, accountService, transactionService, transactionEventBus);

        Account senderAccount = createAccount("Test sender", 100.42, currency);
        Account receiverAccount = createAccount("Test receiver", 0, currency);
        when(accountService.getAccount(senderAccount.getAccountId())).thenReturn(Optional.of(senderAccount));
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        Transaction transaction = createTransaction(senderAccount, receiverAccount, 100.0, TransactionStatus.SUBMITTED);

        when(transactionService.getTransaction(transaction.getTransactionId())).thenReturn(Optional.of(transaction));

        when(accountDao.updateBalance(senderAccount.getAccountId(), 0.42)).thenReturn(true);
        when(transactionDao.updateStatus(transaction.getTransactionId(), TransactionStatus.TRANSIT, null, null)).thenReturn(true);

        when(accountDao.updateBalance(receiverAccount.getAccountId(), 100.)).thenReturn(true);
        when(transactionDao.updateStatus(eq(transaction.getTransactionId()), eq(TransactionStatus.COMPLETED), isNull(), notNull())).thenReturn(true);

        List<TransactionStatus> eventStatuses = new ArrayList<>();
        transactionEventBus.addListener(transactionEvent -> eventStatuses.add(transactionEvent.getStatus()));

        transactionEventBus.transactionStatusChange(transaction);

        transactionProcessingService.shutdown();

        assertEquals(List.of(TransactionStatus.SUBMITTED, TransactionStatus.TRANSIT, TransactionStatus.COMPLETED), eventStatuses);
        assertEquals(0.42, senderAccount.getBalance());
        assertEquals(100, receiverAccount.getBalance());
        assertEquals(TransactionStatus.COMPLETED, transaction.getStatus());
        var processedSecondsAgo = ChronoUnit.SECONDS.between(transaction.getProcessedAt(), LocalDateTime.now());
        assertTrue(processedSecondsAgo < 5, "transaction.processedAt should be within 5s window, was " + processedSecondsAgo + "s");
    }

    @Test
    void shouldDenyTransactionWhenInsufficientFunds() {
        TransactionProcessingService transactionProcessingService = new TransactionProcessingService(properties, jdbi, accountService, transactionService, transactionEventBus);

        Account senderAccount = createAccount("Test sender", 42.12, Currency.USD);
        Account receiverAccount = createAccount("Test receiver", 0, Currency.USD);
        when(accountService.getAccount(senderAccount.getAccountId())).thenReturn(Optional.of(senderAccount));
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        Transaction transaction = createTransaction(senderAccount, receiverAccount, 100.0, TransactionStatus.SUBMITTED);

        when(transactionService.getTransaction(transaction.getTransactionId())).thenReturn(Optional.of(transaction));

        when(transactionDao.updateStatus(eq(transaction.getTransactionId()), eq(TransactionStatus.DENIED), eq(TransactionDenyReason.INSUFFICIENT_FUNDS), notNull()))
                .thenReturn(true);

        List<TransactionStatus> eventStatuses = new ArrayList<>();
        transactionEventBus.addListener(transactionEvent -> eventStatuses.add(transactionEvent.getStatus()));

        transactionEventBus.transactionStatusChange(transaction);

        transactionProcessingService.shutdown();

        assertEquals(List.of(TransactionStatus.SUBMITTED, TransactionStatus.DENIED), eventStatuses);
        assertEquals(42.12, senderAccount.getBalance());
        assertEquals(0, receiverAccount.getBalance());
        assertEquals(TransactionStatus.DENIED, transaction.getStatus());
        var processedSecondsAgo = ChronoUnit.SECONDS.between(transaction.getProcessedAt(), LocalDateTime.now());
        assertTrue(processedSecondsAgo < 5, "transaction.processedAt should be within 5s window, was " + processedSecondsAgo + "s");
    }

    @Test
    void shouldRetryProcessingInCaseOfAtomicUpdateFailure() {
        TransactionProcessingService transactionProcessingService = new TransactionProcessingService(properties, jdbi, accountService, transactionService, transactionEventBus);

        Account senderAccount = createAccount("Test sender", 100.42, Currency.USD);
        Account senderAccountAfterRollback = copy(senderAccount);
        Account receiverAccount = createAccount("Test receiver", 0, Currency.USD);
        when(accountService.getAccount(senderAccount.getAccountId()))
                .thenReturn(Optional.of(senderAccount))
                .thenReturn(Optional.of(senderAccountAfterRollback));
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        Transaction transaction = createTransaction(senderAccount, receiverAccount, 100.0, TransactionStatus.SUBMITTED);
        Transaction transactionAfterRollback = copy(transaction);

        when(transactionService.getTransaction(transaction.getTransactionId()))
                .thenReturn(Optional.of(transaction))
                .thenReturn(Optional.of(transactionAfterRollback));

        when(accountDao.updateBalance(senderAccount.getAccountId(), 0.42))
                .thenReturn(false)
                .thenReturn(true);
        when(transactionDao.updateStatus(transaction.getTransactionId(), TransactionStatus.TRANSIT, null, null)).thenReturn(true);

        when(accountDao.updateBalance(receiverAccount.getAccountId(), 100.)).thenReturn(true);
        when(transactionDao.updateStatus(eq(transaction.getTransactionId()), eq(TransactionStatus.COMPLETED), isNull(), notNull())).thenReturn(true);

        List<TransactionStatus> eventStatuses = new ArrayList<>();
        transactionEventBus.addListener(transactionEvent -> eventStatuses.add(transactionEvent.getStatus()));

        transactionEventBus.transactionStatusChange(transaction);

        transactionProcessingService.shutdown();

        assertEquals(List.of(TransactionStatus.SUBMITTED, TransactionStatus.TRANSIT, TransactionStatus.COMPLETED), eventStatuses);
        assertEquals(0.42, senderAccountAfterRollback.getBalance());
        assertEquals(100., receiverAccount.getBalance());
        assertEquals(TransactionStatus.COMPLETED, transactionAfterRollback.getStatus());
        var processedSecondsAgo = ChronoUnit.SECONDS.between(transactionAfterRollback.getProcessedAt(), LocalDateTime.now());
        assertTrue(processedSecondsAgo < 5, "transaction.processedAt should be within 5s window, was " + processedSecondsAgo + "s");
    }

    @Test
    void shouldEnsureExactlyOnceInCaseOfAcknowledgementFailure() {
        TransactionProcessingService transactionProcessingService = new TransactionProcessingService(properties, jdbi, accountService, transactionService, transactionEventBus);

        Account senderAccount = createAccount("Test sender", 100.42, Currency.USD);
        Account receiverAccount = createAccount("Test receiver", 0, Currency.USD);
        when(accountService.getAccount(senderAccount.getAccountId())).thenReturn(Optional.of(senderAccount));
        when(accountService.getAccount(receiverAccount.getAccountId())).thenReturn(Optional.of(receiverAccount));

        Transaction transaction = createTransaction(senderAccount, receiverAccount, 100.0, TransactionStatus.SUBMITTED);

        when(transactionService.getTransaction(transaction.getTransactionId())).thenReturn(Optional.of(transaction));

        when(accountDao.updateBalance(senderAccount.getAccountId(), 0.42)).thenReturn(true);
        when(transactionDao.updateStatus(transaction.getTransactionId(), TransactionStatus.TRANSIT, null, null)).thenReturn(true);

        when(accountDao.updateBalance(receiverAccount.getAccountId(), 100.)).thenReturn(true);
        when(transactionDao.updateStatus(eq(transaction.getTransactionId()), eq(TransactionStatus.COMPLETED), isNull(), notNull())).thenReturn(true);

        List<TransactionStatus> eventStatuses = new ArrayList<>();
        transactionEventBus.addListener(transactionEvent -> {
            eventStatuses.add(transactionEvent.getStatus());
            if (eventStatuses.size() == 2) {
                // simulate fail during submitting downstream event
                throw new IllegalStateException("expected failure");
            }
        });

        transactionEventBus.transactionStatusChange(transaction);

        transactionProcessingService.shutdown();

        assertEquals(List.of(
                TransactionStatus.SUBMITTED,
                TransactionStatus.TRANSIT,
                // message was delivered second time (but not processed) after acknowledge failure
                TransactionStatus.TRANSIT,
                TransactionStatus.COMPLETED,
                // because of two TRANSIT messages there is second attempt to process it,
                // but transaction state is changed to COMPLETE at the moment of this message
                TransactionStatus.COMPLETED
        ), eventStatuses);
        assertEquals(0.42, senderAccount.getBalance());
        assertEquals(100., receiverAccount.getBalance());
        assertEquals(TransactionStatus.COMPLETED, transaction.getStatus());
        var processedSecondsAgo = ChronoUnit.SECONDS.between(transaction.getProcessedAt(), LocalDateTime.now());
        assertTrue(processedSecondsAgo < 5, "transaction.processedAt should be within 5s window, was " + processedSecondsAgo + "s");
    }

    private Transaction createTransaction(Account senderAccount, Account receiverAccount, double amount, TransactionStatus transactionStatus) {
        var transaction = new Transaction();
        transaction.setTransactionId(UUID.randomUUID().toString());
        transaction.setSenderId(senderAccount.getAccountId());
        transaction.setReceiverId(receiverAccount.getAccountId());
        transaction.setAmount(amount);
        transaction.setStatus(transactionStatus);
        return transaction;
    }

    private Account createAccount(String accountName, double balance, Currency currency) {
        var account = new Account();
        account.setAccountId(UUID.randomUUID().toString());
        account.setAccountName(accountName);
        account.setBalance(balance);
        account.setCurrency(currency);
        return account;
    }

    private Transaction copy(Transaction from) {
        var transaction = new Transaction();
        transaction.setTransactionId(from.getTransactionId());
        transaction.setSenderId(from.getSenderId());
        transaction.setReceiverId(from.getReceiverId());
        transaction.setAmount(from.getAmount());
        transaction.setDescription(from.getDescription());
        transaction.setStatus(from.getStatus());
        transaction.setDenyReason(from.getDenyReason());
        transaction.setCreatedAt(from.getCreatedAt());
        transaction.setProcessedAt(from.getProcessedAt());
        return transaction;
    }

    private Account copy(Account from) {
        var account = new Account();
        account.setAccountId(from.getAccountId());
        account.setAccountName(from.getAccountName());
        account.setBalance(from.getBalance());
        account.setCurrency(from.getCurrency());
        return account;
    }
}
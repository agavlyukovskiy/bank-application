package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.JdbiTest;
import com.github.gavlyukovskiy.bankapplication.domain.Currency;
import com.github.gavlyukovskiy.bankapplication.domain.web.CreateAccountRequest;
import com.github.gavlyukovskiy.bankapplication.exception.HttpException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

class AccountServiceTest extends JdbiTest {

    private AccountService accountService;

    @BeforeEach
    void setUp() {
        accountService = new AccountService(jdbi);
    }

    @ParameterizedTest
    @EnumSource(Currency.class)
    void shouldCreateAccount(Currency currency) {
        var createAccountRequest = new CreateAccountRequest();
        createAccountRequest.setAccountName("Test account");
        createAccountRequest.setCurrency(currency);
        var account = accountService.createAccount(createAccountRequest);

        assertNotNull(account.getAccountId());
        assertEquals("Test account", account.getAccountName());
        assertEquals(currency, account.getCurrency());
        assertEquals(0., account.getBalance());
        var createdMillisAgo = ChronoUnit.MILLIS.between(account.getCreatedAt(), LocalDateTime.now());
        assertTrue(createdMillisAgo < 500, "account.createdAt should be within 500ms window, was " + createdMillisAgo + "ms");

        var retrievedAccount = accountService.getAccount(account.getAccountId());
        assertTrue(retrievedAccount.isPresent());
        assertEquals(account.getAccountId(), retrievedAccount.get().getAccountId());
        assertEquals("Test account", retrievedAccount.get().getAccountName());
        assertEquals(currency, retrievedAccount.get().getCurrency());
        assertEquals(0., retrievedAccount.get().getBalance());
    }

    @Test
    void shouldFailToCreateAccountWhenNameIsNull() {
        var createAccountRequest = new CreateAccountRequest();
        createAccountRequest.setCurrency(Currency.USD);
        var httpException = assertThrows(HttpException.class, () -> accountService.createAccount(createAccountRequest));
        assertEquals(400, httpException.statusCode());
        assertEquals("Account name must be within 50 symbols", httpException.getMessage());
    }

    @Test
    void shouldFailToCreateAccountWhenNameIsTooLong() {
        var createAccountRequest = new CreateAccountRequest();
        createAccountRequest.setAccountName("Lorem Ipsum is simply dummy text of the printing an");
        createAccountRequest.setCurrency(Currency.USD);
        var httpException = assertThrows(HttpException.class, () -> accountService.createAccount(createAccountRequest));
        assertEquals(400, httpException.statusCode());
        assertEquals("Account name must be within 50 symbols", httpException.getMessage());
    }

    @Test
    void shouldFailToCreateAccountWhenCurrencyIsNull() {
        var createAccountRequest = new CreateAccountRequest();
        createAccountRequest.setAccountName("Test account");
        var httpException = assertThrows(HttpException.class, () -> accountService.createAccount(createAccountRequest));
        assertEquals(400, httpException.statusCode());
        assertEquals("Currency must not be null", httpException.getMessage());
    }
}
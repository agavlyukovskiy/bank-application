package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.domain.Transaction;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionStatus;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class TransactionEventBusTest {

    private TransactionEventBus transactionEventBus = new TransactionEventBus();

    @Test
    void shouldReplyEventsToAllListeners() {
        Transaction[] transactionHolder = new Transaction[1];
        transactionEventBus.addListener(transaction -> {
            assertNull(transactionHolder[0], "listener must be called only once");
            transactionHolder[0] = transaction;
        });

        var transaction = new Transaction();
        transaction.setTransactionId(UUID.randomUUID().toString());
        transaction.setStatus(TransactionStatus.SUBMITTED);
        transactionEventBus.transactionStatusChange(transaction);

        assertSame(transaction, transactionHolder[0]);
    }

    @Test
    void shouldStopAcceptingEventsAfterShutdown() {
        transactionEventBus.addListener(transaction -> {
            fail("should not be called");
        });

        transactionEventBus.shutdown();

        var transaction = new Transaction();
        transaction.setTransactionId(UUID.randomUUID().toString());
        transaction.setStatus(TransactionStatus.SUBMITTED);
        assertThrows(IllegalStateException.class, () -> transactionEventBus.transactionStatusChange(transaction));
    }
}
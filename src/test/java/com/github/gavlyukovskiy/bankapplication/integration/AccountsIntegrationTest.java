package com.github.gavlyukovskiy.bankapplication.integration;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountsIntegrationTest extends BaseIntegrationTest {

    @Test
    void shouldCreateAccount() {
        var createAccountResponse = post(api("/accounts"), Map.of(
                "accountName", "test",
                "currency", "USD"
        ));

        assertEquals(200, createAccountResponse.code(), () -> "Expected status 200, response: " + bodyAsString(createAccountResponse));
        var createAccountJson = toJsonNode(createAccountResponse);
        var accountId = createAccountJson.required("accountId").asText();
        assertEquals("test", createAccountJson.required("accountName").asText());
        assertEquals(0., createAccountJson.required("balance").asDouble());
        assertEquals("USD", createAccountJson.required("currency").asText());
        var createdAt = LocalDateTime.parse(createAccountJson.required("createdAt").asText(), DateTimeFormatter.ISO_DATE_TIME);
        var createdSecondsAgo = ChronoUnit.SECONDS.between(createdAt, LocalDateTime.now());
        assertTrue(createdSecondsAgo < 5, "transaction.createdAt should be within 5s window, was " + createdSecondsAgo + "s");

        assertEquals(accountId, createAccountResponse.header("Location"));

        var getAccountResponse = get(api("/accounts/" + accountId));

        assertEquals(200, getAccountResponse.code(), () -> "Expected status 200, response: " + bodyAsString(getAccountResponse));
        var getAccountJson = toJsonNode(getAccountResponse);
        assertEquals(accountId, getAccountJson.required("accountId").asText());
        assertEquals("test", getAccountJson.required("accountName").asText());
        assertEquals(0., getAccountJson.required("balance").asDouble());
        assertEquals("USD", getAccountJson.required("currency").asText());
    }

    @Test
    void shouldFailToCreateAccountWithUnknownCurrency() {
        var createAccountResponse = post(api("/accounts"), Map.of(
                "accountName", "test",
                "currency", "CNY"
        ));

        assertEquals(400, createAccountResponse.code(), () -> "Expected status 200, response: " + bodyAsString(createAccountResponse));
        var createAccountJson = toJsonNode(createAccountResponse);
        assertEquals("Unsupported value [CNY] for type [Currency]", createAccountJson.required("message").asText());
    }

    @Test
    void shouldReturn404WhenNoAccountFound() {
        var getAccountResponse = get(api("/accounts/00000000-0000-0000-0000-000000000000"));

        assertEquals(404, getAccountResponse.code());
        var getAccountJson = toJsonNode(getAccountResponse);
        assertEquals("Account was not found", getAccountJson.required("message").asText());
    }

    @Test
    void shouldReturn404OnWrongRequest() {
        var response = get(api("/accounts"));

        assertEquals(404, response.code());
        var responseJson = toJsonNode(response);
        assertEquals("Not found", responseJson.required("message").asText());
    }
}

package com.github.gavlyukovskiy.bankapplication.integration;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.gavlyukovskiy.bankapplication.domain.Currency;
import lombok.Data;
import okhttp3.Response;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class TransactionsIntegrationTest extends BaseIntegrationTest {

    private static final Logger log = LoggerFactory.getLogger(TransactionsIntegrationTest.class);

    @ParameterizedTest
    @EnumSource(Currency.class)
    void shouldBeAbleToCreateDepositsFromSecureEndpoint(Currency currency) {
        var createAccountResponse = post(api("/accounts"), Map.of(
                "accountName", "test",
                "currency", currency.name()
        ));

        assertEquals(200, createAccountResponse.code(), () -> "Expected status 200, response: " + bodyAsString(createAccountResponse));
        var createAccountJson = toJsonNode(createAccountResponse);
        var accountId = createAccountJson.required("accountId").asText();

        var accountDepositResponse = post(secure("/transactions/deposit"), Map.of(
                "accountId", accountId,
                "amount", 100.42,
                "reason", "Testing deposit call"
        ));

        assertEquals(200, accountDepositResponse.code(), () -> "Expected status 200, response: " + bodyAsString(accountDepositResponse));
        JsonNode accountDepositJson = toJsonNode(accountDepositResponse);
        String transactionId = accountDepositJson.required("transactionId").asText();
        assertEquals("00000000-0000-0000-0000-000000000000", accountDepositJson.required("senderId").asText());
        assertEquals(accountId, accountDepositJson.required("receiverId").asText());
        assertEquals(100.42, accountDepositJson.required("amount").asDouble());

        var createdAt = LocalDateTime.parse(accountDepositJson.required("createdAt").asText(), DateTimeFormatter.ISO_DATE_TIME);
        var createdSecondsAgo = ChronoUnit.SECONDS.between(createdAt, LocalDateTime.now());
        assertTrue(createdSecondsAgo < 5, "transaction.createdAt should be within 5s window, was " + createdSecondsAgo + "s");

        waitAndVerifyTransaction(transactionId, transaction -> {
            var transactionStatus = transaction.required("status").asText();
            if (transactionStatus.equals("COMPLETED")) {
                assertFalse(transaction.has("reason"));

                var processedAt = LocalDateTime.parse(transaction.required("processedAt").asText(), DateTimeFormatter.ISO_DATE_TIME);
                var processedSecondsAgo = ChronoUnit.SECONDS.between(processedAt, LocalDateTime.now());
                assertTrue(processedSecondsAgo < 5, "transaction.processedAt should be within 5s window, was " + processedSecondsAgo + "s");

                var receiverBalance = requestAccountBalance(accountId);

                assertEquals(100.42, receiverBalance);
            } else {
                fail("Unexpected transaction status [" + transactionStatus + "]");
            }
        });
    }

    @ParameterizedTest
    @EnumSource(Currency.class)
    void shouldTransferMoneyBetweenAccounts(Currency currency) {
        var senderAccountId = setupAccount("Sender", currency.name(), 1000.);
        var receiverAccountId = setupAccount("Receiver", currency.name(), 0.);

        var submitTransactionResponse = post(api("/transactions"), Map.of(
                "senderId", senderAccountId,
                "receiverId", receiverAccountId,
                "amount", 100.42,
                "description", "This test transaction should succeed"
        ));

        assertEquals(200, submitTransactionResponse.code(), () -> "Expected status 200, response: " + bodyAsString(submitTransactionResponse));
        var submitTransactionJson = toJsonNode(submitTransactionResponse);
        var transactionId = submitTransactionJson.required("transactionId").asText();
        assertEquals(senderAccountId, submitTransactionJson.required("senderId").asText());
        assertEquals(receiverAccountId, submitTransactionJson.required("receiverId").asText());
        assertEquals(100.42, submitTransactionJson.required("amount").asDouble());

        var createdAt = LocalDateTime.parse(submitTransactionJson.required("createdAt").asText(), DateTimeFormatter.ISO_DATE_TIME);
        var createdSecondsAgo = ChronoUnit.SECONDS.between(createdAt, LocalDateTime.now());
        assertTrue(createdSecondsAgo < 5, "transaction.createdAt should be within 5s window, was " + createdSecondsAgo + "s");

        waitAndVerifyTransaction(transactionId, transaction -> {
            var transactionStatus = transaction.required("status").asText();
            if (transactionStatus.equals("COMPLETED")) {
                assertFalse(transaction.has("reason"));

                var processedAt = LocalDateTime.parse(transaction.required("processedAt").asText(), DateTimeFormatter.ISO_DATE_TIME);
                var processedSecondsAgo = ChronoUnit.SECONDS.between(processedAt, LocalDateTime.now());
                assertTrue(processedSecondsAgo < 5, "transaction.processedAt should be within 5s window, was " + processedSecondsAgo + "s");

                var senderBalance = requestAccountBalance(senderAccountId);
                var receiverBalance = requestAccountBalance(receiverAccountId);

                assertEquals(899.58, senderBalance);
                assertEquals(100.42, receiverBalance);
            } else {
                fail("Unexpected transaction status [" + transactionStatus + "]");
            }
        });
    }

    @Test
    void shouldDenyTransactionsForAccountsWithDifferentCurrencies() {
        var senderAccountId = setupAccount("Sender", "USD", 1000.);
        var receiverAccountId = setupAccount("Receiver", "EUR", 0.);

        var submitTransactionResponse = post(api("/transactions"), Map.of(
                "senderId", senderAccountId,
                "receiverId", receiverAccountId,
                "amount", 100.42,
                "description", "This transaction should fail due to USD -> EUR transition"
        ));

        assertEquals(400, submitTransactionResponse.code(), () -> "Expected status 200, response: " + bodyAsString(submitTransactionResponse));
        JsonNode submitTransactionJson = toJsonNode(submitTransactionResponse);
        assertEquals("Transfer between accounts with different currencies is not supported", submitTransactionJson.required("message").asText());
    }

    @Test
    void shouldDenyTransactionsWithInvalidAmount() {
        var senderAccountId = setupAccount("Sender", "USD", 1000.);
        var receiverAccountId = setupAccount("Receiver", "USD", 0.);

        var submitTransactionResponse = post(api("/transactions"), Map.of(
                "senderId", senderAccountId,
                "receiverId", receiverAccountId,
                "amount", "this is string field, right?",
                "description", "This transaction should fail due to invalid amount type"
        ));

        assertEquals(400, submitTransactionResponse.code(), () -> "Expected status 200, response: " + bodyAsString(submitTransactionResponse));
        JsonNode submitTransactionJson = toJsonNode(submitTransactionResponse);
        assertEquals("Unsupported value [this is string field, right?] for type [double]", submitTransactionJson.required("message").asText());
    }

    @Test
    void shouldFailTwoTransactionsWhenNoSufficientFunds() {
        // making 98 concurrent transactions with amount 102, total cost = 10200,
        // all transactions should be submitted but two must be failed on processing side due to insufficient funds, 10000 - 98 * 102 = 4
        var senderAccountId = setupAccount("Sender", "USD", 10000.);
        var receiverAccountId = setupAccount("Receiver", "USD", 0.);

        var forkJoinPool = new ForkJoinPool(5);

        var transactions = IntStream.range(0, 100)
                .mapToObj(i -> CompletableFuture.supplyAsync(() -> {
                    var submitTransactionResponse = post(api("/transactions"), Map.of(
                            "senderId", senderAccountId,
                            "receiverId", receiverAccountId,
                            "amount", 102,
                            "description", "Transaction #" + i
                    ));
                    assertEquals(200, submitTransactionResponse.code(), () -> "Expected status 200, response: " + bodyAsString(submitTransactionResponse));
                    var submitTransactionJson = toJsonNode(submitTransactionResponse);
                    return new TransactionWithRequestId(i, submitTransactionJson.required("transactionId").asText());
                }, forkJoinPool))
                .collect(Collectors.toList())
                .stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());

        AtomicInteger completedTransactions = new AtomicInteger();
        AtomicInteger deniedTransactions = new AtomicInteger();
        transactions.forEach(transactionWithRequestId -> {
            waitAndVerifyTransaction(transactionWithRequestId.getTransactionId(), transaction -> {
                var transactionStatus = transaction.required("status").asText();
                if (transactionStatus.equals("COMPLETED")) {
                    assertFalse(transaction.has("reason"));
                    completedTransactions.incrementAndGet();
                } else if (transactionStatus.equals("DENIED")) {
                    assertEquals("INSUFFICIENT_FUNDS", transaction.required("denyReason").asText());
                    deniedTransactions.incrementAndGet();
                }
                var createdAt = LocalDateTime.parse(transaction.required("createdAt").asText(), DateTimeFormatter.ISO_DATE_TIME);
                var processedAt = LocalDateTime.parse(transaction.required("processedAt").asText(), DateTimeFormatter.ISO_DATE_TIME);
                var processTook = ChronoUnit.SECONDS.between(createdAt, processedAt);
                assertTrue(processTook < 5, "transaction should be processed within 5s window, was " + processTook + "s");
            });
        });

        assertEquals(100, transactions.size());
        assertEquals(98, completedTransactions.get());
        assertEquals(2, deniedTransactions.get());

        var senderBalance = requestAccountBalance(senderAccountId);
        var receiverBalance = requestAccountBalance(receiverAccountId);

        assertEquals(4, senderBalance);
        assertEquals(9996, receiverBalance);
    }

    @Test
    void shouldDisplayHealthCheckAfterTransfer() {
        setupAccount("Sender", "USD", 1000.);

        var healthResponse = get(secure("/health"));

        assertEquals(200, healthResponse.code());
        var healthResponseJson = toJsonNode(healthResponse);
        assertTrue(healthResponseJson.required("db").required("healthy").booleanValue());
        assertTrue(healthResponseJson.required("transactionProcessing").required("healthy").booleanValue());

        long processedEventsTotal = 0;
        for (JsonNode processorInfo : healthResponseJson.required("transactionProcessing").required("info")) {
            assertEquals(0, processorInfo.required("queueSize").asInt());
            processedEventsTotal += processorInfo.required("processedEvents").asLong();
        }
        // processed 1 TRANSIT event during account setup
        assertEquals(1, processedEventsTotal);
    }

    private void waitAndVerifyTransaction(String transactionId, Consumer<JsonNode> transactionConsumer) {
        var start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start <= 5000) {
            JsonNode transaction = requestTransaction(transactionId);
            var transactionStatus = transaction.required("status").asText();
            if (transactionStatus.equals("SUBMITTED") || transactionStatus.equals("TRANSIT")) {
                assertFalse(transaction.hasNonNull("processedAt"));

                log.info("Transaction is in status [{}], waiting 100ms to retry...", transactionStatus);
                safeSleep(100);;
            } else {
                transactionConsumer.accept(transaction);
                return;
            }
        }
        fail("Transaction should be completed within 5s");
    }

    private String setupAccount(String accountName, String currency, double balance) {
        var createAccountResponse = post(api("/accounts"), Map.of(
                "accountName", accountName,
                "currency", currency
        ));

        assertEquals(200, createAccountResponse.code(), () -> "Expected status 200, response: " + bodyAsString(createAccountResponse));
        JsonNode account = toJsonNode(createAccountResponse);
        String accountId = account.required("accountId").asText();
        if (balance > 0) {
            var accountDepositResponse = post(secure("/transactions/deposit"), Map.of(
                    "accountId", accountId,
                    "amount", balance,
                    "reason", "Initial balance for test"
            ));
            assertEquals(200, accountDepositResponse.code(), () -> "Expected status 200, response: " + bodyAsString(accountDepositResponse));
            var accountDepositJson = toJsonNode(accountDepositResponse);
            String transactionId = accountDepositJson.required("transactionId").asText();
            waitAndVerifyTransaction(transactionId, transaction -> {
                assertEquals("COMPLETED", transaction.required("status").asText());
            });
        }
        return accountId;
    }

    private double requestAccountBalance(String accountId) {
        var getAccountResponse = get(api("/accounts/" + accountId));

        assertEquals(200, getAccountResponse.code(), "failed to check account balance");
        JsonNode account = toJsonNode(getAccountResponse);
        return account.required("balance").require().asDouble();
    }

    private JsonNode requestTransaction(String transactionId) {
        var getTransactionResponse = get(api("/transactions/" + transactionId));

        assertEquals(200, getTransactionResponse.code(), "failed to get transaction");
        return toJsonNode(getTransactionResponse);
    }

    @Data
    private static class TransactionWithRequestId {
        private final int requestId;
        private final String transactionId;
    }
}

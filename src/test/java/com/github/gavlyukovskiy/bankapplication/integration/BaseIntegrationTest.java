package com.github.gavlyukovskiy.bankapplication.integration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.gavlyukovskiy.bankapplication.Application;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Objects;

public class BaseIntegrationTest {
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    private Application application;

    protected final OkHttpClient client = new OkHttpClient();
    protected final ObjectMapper objectMapper = new ObjectMapper()
                    .registerModule(new JavaTimeModule())
                    .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    @BeforeEach
    void setUp() {
        application = new Application();
        application.start(0, 0);
    }

    @AfterEach
    void tearDown() {
        application.stop();
    }

    protected Response get(String path) {
        try {
            return client.newCall(
                    new Builder()
                            .get()
                            .url(path)
                            .build())
                    .execute();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected Response post(String path, Object body) {
        try {
            return client.newCall(
                    new Builder()
                            .post(RequestBody.create(objectMapper.writeValueAsString(body), JSON))
                            .url(path)
                            .build())
                    .execute();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected JsonNode toJsonNode(Response response) {
        try {
            var body = response.body();
            Objects.requireNonNull(body, "body is null");
            return objectMapper.readTree(body.string());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected String bodyAsString(Response response) {
        try {
            var body = response.body();
            return body != null ? body.string() : "";
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected String api(String path) {
        return "http://localhost:" + application.apiPort() + "/api" + path;
    }

    protected String secure(String path) {
        return "http://localhost:" + application.securePort() + "/secure" + path;
    }

    protected void safeSleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}

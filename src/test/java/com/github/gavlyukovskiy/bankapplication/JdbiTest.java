package com.github.gavlyukovskiy.bankapplication;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.extension.ExtensionFactory;
import org.jdbi.v3.core.extension.HandleSupplier;
import org.jdbi.v3.core.spi.JdbiPlugin;
import org.jdbi.v3.sqlobject.SqlObjectFactory;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.junit.jupiter.api.BeforeEach;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;

public class JdbiTest {

    protected Jdbi jdbi;

    private final Map<Class<?>, Object> jdbiMocks = new HashMap<>();

    @BeforeEach
    void setUpJdbi() {
        jdbi = Jdbi.create("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        jdbi.installPlugin(new SqlObjectPlugin());
        jdbi.installPlugin(new JdbiPlugin() {
            @Override
            public void customizeJdbi(Jdbi jdbi) {
                jdbi.registerExtension(new ExtensionFactory() {
                    @Override
                    public boolean accepts(Class<?> extensionType) {
                        return jdbiMocks.containsKey(extensionType);
                    }

                    @Override
                    @SuppressWarnings("unchecked")
                    public <E> E attach(Class<E> extensionType, HandleSupplier handle) {
                        return (E) jdbiMocks.get(extensionType);
                    }
                });
            }
        });

        try (var importSqlStream = Application.class.getResourceAsStream("/import.sql")) {
            var importSql = IOUtils.toString(importSqlStream);
            jdbi.useHandle(h -> {
                h.execute("drop table if exists `accounts`");
                h.execute("drop table if exists `transactions`");
                h.execute(importSql);
            });
        } catch (IOException e) {
            throw new UncheckedIOException("Failed to load import.sql", e);
        }
    }

    protected <T> void addJdbiMockDao(Class<T> daoClass, T mockDao) {
        jdbiMocks.put(daoClass, mockDao);
    }
}

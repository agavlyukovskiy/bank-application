package com.github.gavlyukovskiy.bankapplication.validation;

import com.github.gavlyukovskiy.bankapplication.exception.HttpException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GenericValidatorTest {
    @Test
    void shouldThrowExceptionWhenFalse() {
        var httpException = assertThrows(HttpException.class, () -> GenericValidator.validate(false, "test"));
        assertEquals(400, httpException.statusCode());
        assertEquals("test", httpException.getMessage());
    }
    @Test
    void shouldDoNothingWhenTrue() {
        GenericValidator.validate(true, "test");
    }
}
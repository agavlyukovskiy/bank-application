package com.github.gavlyukovskiy.bankapplication.dao;

import com.github.gavlyukovskiy.bankapplication.domain.Account;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.Optional;

@RegisterBeanMapper(Account.class)
public interface AccountDao {

    @SqlUpdate("insert into accounts values (:accountId, :accountName, :balance, :currency, :createdAt)")
    void insert(@BindBean Account account);

    @SqlQuery("select * from accounts where accountId = :accountId")
    Optional<Account> getAccount(@Bind("accountId") String accountId);

    @SqlUpdate("update accounts set balance = :balance where accountId = :accountId")
    boolean updateBalance(@Bind("accountId") String accountId, @Bind("balance") double balance);
}

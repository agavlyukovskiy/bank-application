package com.github.gavlyukovskiy.bankapplication.dao;

import com.github.gavlyukovskiy.bankapplication.domain.Transaction;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionDenyReason;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionStatus;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.time.LocalDateTime;
import java.util.Optional;

@RegisterBeanMapper(Transaction.class)
public interface TransactionDao {

    @SqlUpdate("insert into `transactions` values (:transactionId, :senderId, :receiverId, :amount, :description, :status, :denyReason, :createdAt, :processedAt)")
    void insert(@BindBean Transaction transaction);

    @SqlUpdate("update `transactions` set status = :status, denyReason = :denyReason, processedAt = :processedAt where transactionId = :transactionId")
    boolean updateStatus(
            @Bind("transactionId") String transactionId,
            @Bind("status") TransactionStatus status,
            @Bind("denyReason") TransactionDenyReason denyReason,
            @Bind("processedAt") LocalDateTime processedAt
    );

    @SqlQuery("select * from `transactions` where transactionId = :transactionId")
    Optional<Transaction> getTransaction(@Bind("transactionId") String transactionId);
}

package com.github.gavlyukovskiy.bankapplication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.gavlyukovskiy.bankapplication.domain.web.CreateAccountRequest;
import com.github.gavlyukovskiy.bankapplication.domain.web.DepositRequest;
import com.github.gavlyukovskiy.bankapplication.domain.web.SubmitTransactionRequest;
import com.github.gavlyukovskiy.bankapplication.exception.HttpException;
import com.github.gavlyukovskiy.bankapplication.service.AccountService;
import com.github.gavlyukovskiy.bankapplication.service.HealthService;
import com.github.gavlyukovskiy.bankapplication.service.TransactionEventBus;
import com.github.gavlyukovskiy.bankapplication.service.TransactionProcessingService;
import com.github.gavlyukovskiy.bankapplication.service.TransactionService;
import com.zaxxer.hikari.HikariDataSource;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Service;
import spark.route.HttpMethod;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import static com.github.gavlyukovskiy.bankapplication.Constants.APPLICATION_JSON;

public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);
    private Service apiService;
    private Service secureService;
    private TransactionProcessingService transactionProcessingService;
    private TransactionEventBus transactionEventBus;
    private HealthService healthService;

    public static void main(String[] args) {
        new Application().start(8080, 8081);
    }

    private ObjectMapper objectMapper;
    private AccountService accountService;
    private TransactionService transactionService;

    public void start(int apiPort, int adminPort) {
        initModules();
        apiService = startApiService(apiPort);
        secureService = startAdminService(adminPort);

        apiService.awaitInitialization();
        secureService.awaitInitialization();
        log.info("Api service started on port [{}]", apiPort());
        log.info("Secure service started on port [{}]", securePort());
    }

    public void stop() {
        transactionEventBus.shutdown();
        transactionProcessingService.shutdown();

        apiService.stop();
        secureService.stop();

        apiService.awaitStop();
        secureService.awaitStop();
    }

    public int apiPort() {
        return apiService.port();
    }

    public int securePort() {
        return secureService.port();
    }

    private void initModules() {
        objectMapper = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        Properties properties = loadProperties();

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(Objects.requireNonNull(properties.getProperty("datasource.jdbcUrl")));
        dataSource.setUsername(properties.getProperty("datasource.username", ""));
        dataSource.setPassword(properties.getProperty("datasource.password", ""));

        var jdbi = Jdbi.create(dataSource);
        jdbi.installPlugin(new SqlObjectPlugin());

        try (var importSqlStream = Application.class.getResourceAsStream("/import.sql")) {
            var importSql = IOUtils.toString(importSqlStream);
            jdbi.useHandle(h -> h.execute(importSql));
        } catch (IOException e) {
            throw new UncheckedIOException("Failed to load import.sql", e);
        }

        accountService = new AccountService(jdbi);
        transactionEventBus = new TransactionEventBus();
        transactionService = new TransactionService(jdbi, accountService, transactionEventBus);
        transactionProcessingService = new TransactionProcessingService(properties, jdbi, accountService, transactionService, transactionEventBus);
        healthService = new HealthService(jdbi, transactionProcessingService);
    }

    private Properties loadProperties() {
        var properties = new Properties();
        try (var propertiesStream = Application.class.getResourceAsStream("/application.properties")) {
            properties.load(propertiesStream);
            if (Files.exists(Paths.get("application.properties"))) {
                try (var filePropertiesStream = Files.newInputStream(Paths.get("application.properties"))) {
                    properties.load(filePropertiesStream);
                }
            }
        } catch (IOException e) {
            throw new UncheckedIOException("Failed to load application.properties", e);
        }
        return properties;
    }

    private Service startApiService(int port) {
        var service = Service.ignite();
        service.port(port);

        setupCommonRoutes(service);

        service.post("/api/accounts", APPLICATION_JSON, (request, response) -> {
            var createAccountRequest = objectMapper.readValue(request.body(), CreateAccountRequest.class);
            var account = accountService.createAccount(createAccountRequest);
            response.header("Location", account.getAccountId());
            return account;
        }, objectMapper::writeValueAsString);

        service.get("/api/accounts/:id", APPLICATION_JSON, (request, response) -> {
            var accountId = request.params("id");
            return accountService.getAccount(accountId).orElseThrow(() -> new HttpException(404, "Account was not found"));
        }, objectMapper::writeValueAsString);

        service.post("/api/transactions", APPLICATION_JSON, (request, response) -> {
            var submitTransactionRequest = objectMapper.readValue(request.body(), SubmitTransactionRequest.class);
            var transaction = transactionService.submitTransaction(submitTransactionRequest, false);
            response.header("Location", transaction.getTransactionId());
            return transaction;
        }, objectMapper::writeValueAsString);

        service.get("/api/transactions/:id", APPLICATION_JSON, (request, response) -> {
            var transactionId = request.params("id");
            return transactionService.getTransaction(transactionId).orElseThrow(() -> new HttpException(404, "Transaction was not found"));
        }, objectMapper::writeValueAsString);

        return service;
    }

    private Service startAdminService(int port) {
        var service = Service.ignite();
        service.port(port);

        setupCommonRoutes(service);

        service.post("/secure/transactions/deposit", APPLICATION_JSON, (request, response) -> {
            var setAccountBalanceRequest = objectMapper.readValue(request.body(), DepositRequest.class);
            return transactionService.makeDeposit(setAccountBalanceRequest);
        }, objectMapper::writeValueAsString);

        service.get("/secure/health", APPLICATION_JSON, (request, response) -> healthService.healthCheck(), objectMapper::writeValueAsString);

        return service;
    }

    private void setupCommonRoutes(Service service) {
        service.before((request, response) -> {
            response.type(APPLICATION_JSON);
        });

        service.internalServerError((request, response) -> "{\"message\": \"Internal Server Error\"}");

        service.notFound((request, response) -> "{\"message\": \"Not found\"}");

        service.exception(InvalidFormatException.class, (e, request, response) -> {
            try {
                response.status(400);
                var message = "Unsupported value [" + e.getValue() + "] for type [" + e.getTargetType().getSimpleName() + "]";
                response.body(objectMapper.writeValueAsString(Map.of("message", message)));
            } catch (JsonProcessingException ex) {
                log.error("Failed to write correct response", ex);
            }
        });

        service.exception(HttpException.class, (e, request, response) -> {
            try {
                response.status(e.statusCode());
                response.body(objectMapper.writeValueAsString(Map.of("message", e.getMessage())));
            } catch (JsonProcessingException ex) {
                log.error("Failed to write correct response", ex);
            }
        });
    }
}

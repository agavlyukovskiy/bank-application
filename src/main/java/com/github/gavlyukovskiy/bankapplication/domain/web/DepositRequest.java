package com.github.gavlyukovskiy.bankapplication.domain.web;

import lombok.Data;

@Data
public class DepositRequest {
    private String accountId;
    private double amount;
    private String reason;
}

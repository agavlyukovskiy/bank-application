package com.github.gavlyukovskiy.bankapplication.domain;

/**
 * Represents current transaction status.
 */
public enum TransactionStatus {
    /**
     * Transaction was submitted for processing and still in the queue.
     *
     * Status may be changed to
     * {@link #TRANSIT} if transaction was validated and money were withdrawn from sender's account.
     * {@link #DENIED} if transaction was not validated.
     */
    SUBMITTED,
    /**
     * Transaction was processed by the worker and money were withdrawn from sender's account,
     * but wasn't yet delivered to receiver account.
     *
     * Status may be changed to
     * {@link #COMPLETED} once money are available on receiver's account.
     */
    TRANSIT,
    /**
     * Transaction was denied.
     *
     * @see Transaction#getDenyReason()
     */
    DENIED,
    /**
     * Transaction was completed successfully, accounts' balances were updated.
     */
    COMPLETED
}

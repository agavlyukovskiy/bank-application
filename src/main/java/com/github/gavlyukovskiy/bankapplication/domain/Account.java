package com.github.gavlyukovskiy.bankapplication.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Account {
    private String accountId;
    private String accountName;
    private double balance;
    private Currency currency;
    private LocalDateTime createdAt;
}

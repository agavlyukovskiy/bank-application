package com.github.gavlyukovskiy.bankapplication.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

import java.util.Map;

@Data
public class HealthCheck {
    private final boolean healthy;
    @JsonInclude(Include.NON_EMPTY)
    private final Map<String, Object> info;
}

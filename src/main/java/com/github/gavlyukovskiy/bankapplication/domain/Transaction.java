package com.github.gavlyukovskiy.bankapplication.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Transaction {
    private String transactionId;
    private String senderId;
    private String receiverId;
    private double amount;
    private String description;
    private TransactionStatus status;
    @JsonInclude(Include.NON_NULL)
    private TransactionDenyReason denyReason;
    private LocalDateTime createdAt;
    private LocalDateTime processedAt;
}

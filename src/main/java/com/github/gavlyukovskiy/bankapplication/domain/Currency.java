package com.github.gavlyukovskiy.bankapplication.domain;

public enum Currency {
    USD,
    EUR
}

package com.github.gavlyukovskiy.bankapplication.domain;

import lombok.Data;

@Data
public class TransactionProcessingEvent {
    private final String transactionId;
    /**
     * Transaction status at the moment of event submission, used to ensure exactly-once relationship.
     */
    private final TransactionStatus status;

    public TransactionProcessingEvent(Transaction transaction) {
        this.transactionId = transaction.getTransactionId();
        this.status = transaction.getStatus();
    }
}

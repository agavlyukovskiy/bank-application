package com.github.gavlyukovskiy.bankapplication.domain.web;

import com.github.gavlyukovskiy.bankapplication.domain.Currency;
import lombok.Data;

@Data
public class CreateAccountRequest {
    private String accountName;
    private Currency currency;
}

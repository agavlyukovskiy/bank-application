package com.github.gavlyukovskiy.bankapplication.domain;

public enum TransactionDenyReason {
    /**
     * Transaction was denied due to insufficient funds at the moment of processing.
     * May happen if two transactions were submitted simultaneously and balance was
     * sufficient for each transaction, but not both.
     */
    INSUFFICIENT_FUNDS,
    /**
     * There was unexpected error during transaction processing. This transaction must be corrected manually.
     */
    INTERNAL_ERROR
}

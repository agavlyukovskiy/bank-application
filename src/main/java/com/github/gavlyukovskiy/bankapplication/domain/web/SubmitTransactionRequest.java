package com.github.gavlyukovskiy.bankapplication.domain.web;

import lombok.Data;

@Data
public class SubmitTransactionRequest {
    private String senderId;
    private String receiverId;
    private double amount;
    private String description;
}

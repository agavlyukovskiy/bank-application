package com.github.gavlyukovskiy.bankapplication.validation;

import com.github.gavlyukovskiy.bankapplication.exception.HttpException;

/**
 * Custom validator, may be replaced with Hibernate Validator.
 */
public class GenericValidator {

    public static void validate(boolean state, String message) {
        if (!state) {
            throw new HttpException(400, message);
        }
    }
}

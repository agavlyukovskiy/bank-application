package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.dao.AccountDao;
import com.github.gavlyukovskiy.bankapplication.dao.TransactionDao;
import com.github.gavlyukovskiy.bankapplication.domain.Account;
import com.github.gavlyukovskiy.bankapplication.domain.Transaction;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionDenyReason;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionProcessingEvent;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionStatus;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;

public class TransactionProcessor {
    private static final Logger log = LoggerFactory.getLogger(TransactionProcessor.class);

    private final String id;
    private final Queue<TransactionProcessingEvent> queue;
    private final ExecutorService executorService;
    private final TransactionProcessingWorker worker;
    private final Future<?> future;

    TransactionProcessor(
            int index,
            int submittedQueueSize,
            Jdbi jdbi,
            TransactionEventBus transactionEventBus,
            AccountService accountService,
            TransactionService transactionService
    ) {
        this.id = "tx-worker-" + index;
        this.executorService = Executors.newSingleThreadExecutor(r -> {
            var thread = new Thread(r);
            thread.setDaemon(false);
            thread.setName(this.id);
            return thread;
        });
        this.queue = new ArrayBlockingQueue<>(submittedQueueSize);
        this.worker = new TransactionProcessingWorker(queue, jdbi, transactionEventBus, accountService, transactionService);
        this.future = executorService.submit(worker);
    }

    public String getId() {
        return id;
    }

    public boolean isAlive() {
        return !future.isCancelled() && !future.isDone();
    }

    public int getQueueSize() {
        return queue.size();
    }

    public long getProcessedEvents() {
        return worker.getProcessedEvents();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public void offer(Transaction transaction) {
        while (!queue.offer(new TransactionProcessingEvent(transaction))) {
            // retrying until accepted, theoretically some back-off could be set up here
            // alternatively we might create job that will get all SUBMITTED transaction from the database and resubmit them
        }
    }

    public void shutdown() {
        log.info("Shutting down processor [{}], queue size [{}]", id, queue.size());
        future.cancel(true);
        executorService.shutdown();
    }

    static class TransactionProcessingWorker implements Runnable {

        private static final Logger log = LoggerFactory.getLogger(TransactionProcessingWorker.class);

        private final Queue<TransactionProcessingEvent> transactionsQueue;
        private final Jdbi jdbi;
        private final TransactionEventBus transactionEventBus;
        private final AccountService accountService;
        private final TransactionService transactionService;
        private final AtomicLong processedEvents = new AtomicLong();

        TransactionProcessingWorker(
                Queue<TransactionProcessingEvent> transactionsQueue,
                Jdbi jdbi,
                TransactionEventBus transactionEventBus,
                AccountService accountService,
                TransactionService transactionService
        ) {
            this.transactionsQueue = transactionsQueue;
            this.jdbi = jdbi;
            this.transactionEventBus = transactionEventBus;
            this.accountService = accountService;
            this.transactionService = transactionService;
        }

        @Override
        public final void run() {
            while (!Thread.currentThread().isInterrupted()) {
                var transactionId = transactionsQueue.peek();
                if (transactionId == null) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        return;
                    }
                    continue;
                }
                try {
                    nextMessage(transactionId);
                } catch (RuntimeException e) {
                    // processing fail will not acknowledge message and will try processing until succeeds
                    // it might be replaced with more sophisticated approach to set transaction into some INTERNAL_ERROR state
                    // after some amount of retries or in case of corrupted state, which then must be reported and reviewed manually
                    log.error("Error during transaction processing", e);
                }
            }
        }

        private void nextMessage(TransactionProcessingEvent transactionProcessingEvent) {
            var transactionId = transactionProcessingEvent.getTransactionId();
            var eventTransactionStatus = transactionProcessingEvent.getStatus();
            var transaction = transactionService.getTransaction(transactionId)
                    .orElseThrow(() -> new IllegalStateException("Transaction [" + transactionId + "] does not exist"));

            var transactionStatus = transaction.getStatus();
            if (transactionStatus != eventTransactionStatus) {
                // transaction status was changed by this exact worker, but acknowledgement was failed
                // resending status change because it might have failed last time
                log.info("Acknowledging an event because status of transaction [{}] was changed from [{}] to [{}]", transaction.getTransactionId(),
                        eventTransactionStatus, transactionStatus);
                transactionEventBus.transactionStatusChange(transaction);
                acknowledge();
            } else if (transactionStatus == TransactionStatus.SUBMITTED) {
                log.info("Processing submitted transaction [{}], sender [{}], receiver [{}]", transaction.getTransactionId(),
                        transaction.getSenderId(), transaction.getReceiverId());
                processSubmittedTransaction(transaction);
            } else if (transactionStatus == TransactionStatus.TRANSIT) {
                log.info("Processing transit transaction [{}], sender [{}], receiver [{}]", transaction.getTransactionId(),
                        transaction.getSenderId(), transaction.getReceiverId());
                processTransitTransaction(transaction);
            }
        }

        private void processSubmittedTransaction(Transaction transaction) {
            var senderAccount = accountService.getAccount(transaction.getSenderId())
                    .orElseThrow(() -> new IllegalStateException("Sender account does not exists"));
            var receiverAccount = accountService.getAccount(transaction.getReceiverId())
                    .orElseThrow(() -> new IllegalStateException("Receiver account does not exists"));
            validateTransaction(transaction, senderAccount, receiverAccount);

            if (senderAccount.getBalance() < transaction.getAmount()) {
                transaction.setDenyReason(TransactionDenyReason.INSUFFICIENT_FUNDS);
                transaction.setStatus(TransactionStatus.DENIED);
                transaction.setProcessedAt(LocalDateTime.now());
                jdbi.useTransaction(handle -> {
                    var transactionDao = handle.attach(TransactionDao.class);
                    var transactionUpdated = transactionDao.updateStatus(transaction.getTransactionId(), transaction.getStatus(),
                            transaction.getDenyReason(), transaction.getProcessedAt());
                    if (!transactionUpdated) {
                        throw new IllegalStateException("Failed to update transaction");
                    }
                });

                transactionEventBus.transactionStatusChange(transaction);

                log.info("Transaction [{}, {}] was denied due to insufficient funds on sender account [{}, {}]",
                        transaction.getTransactionId(), transaction.getAmount(), senderAccount.getAccountId(), senderAccount.getBalance());
            } else {
                var senderAccountBalance = withRounding(senderAccount.getBalance(), transaction.getAmount(), BigDecimal::subtract);
                senderAccount.setBalance(senderAccountBalance);
                transaction.setStatus(TransactionStatus.TRANSIT);

                updateAccountAndTransaction(transaction, senderAccount);

                transactionEventBus.transactionStatusChange(transaction);

                log.info("Money were transferred from sender account [{}, {}] and transaction [{}, {}] state was changed to TRANSIT",
                        senderAccount.getAccountId(), senderAccountBalance, transaction.getTransactionId(), transaction.getAmount());
            }

            acknowledge();
        }

        private void processTransitTransaction(Transaction transaction) {
            var receiverAccount = accountService.getAccount(transaction.getReceiverId())
                    .orElseThrow(() -> new IllegalStateException("Receiver account does not exists"));

            var receiverAccountBalance = withRounding(receiverAccount.getBalance(), transaction.getAmount(), BigDecimal::add);
            receiverAccount.setBalance(receiverAccountBalance);
            transaction.setStatus(TransactionStatus.COMPLETED);
            transaction.setProcessedAt(LocalDateTime.now());

            updateAccountAndTransaction(transaction, receiverAccount);

            transactionEventBus.transactionStatusChange(transaction);

            log.info("Money were transferred to receiver account [{}, {}] and transaction [{}, {}] state was changed to COMPLETED",
                    receiverAccount.getAccountId(), receiverAccountBalance, transaction.getTransactionId(), transaction.getAmount());

            acknowledge();
        }

        private void validateTransaction(Transaction transaction, Account senderAccount, Account receiverAccount) {
            if (transaction.getAmount() <= 0 || new BigDecimal(transaction.getAmount() * 100).scale() != 0) {
                throw new IllegalStateException("Amount is invalid");
            }
            if (senderAccount.getAccountId().equals(receiverAccount.getAccountId())) {
                throw new IllegalStateException("Cannot transfer money to the same account");
            }
            if (senderAccount.getCurrency() != receiverAccount.getCurrency()) {
                throw new IllegalStateException("Accounts currency doesn't match");
            }
        }

        private void updateAccountAndTransaction(Transaction transaction, Account senderAccount) {
            jdbi.useTransaction(handle -> {
                var accountDao = handle.attach(AccountDao.class);
                var transactionDao = handle.attach(TransactionDao.class);

                var accountUpdated = accountDao.updateBalance(senderAccount.getAccountId(), senderAccount.getBalance());
                var transactionUpdated = transactionDao.updateStatus(transaction.getTransactionId(), transaction.getStatus(),
                        transaction.getDenyReason(), transaction.getProcessedAt());

                if (!accountUpdated || !transactionUpdated) {
                    throw new IllegalStateException("Failed to update account and transaction");
                }
            });
        }

        private void acknowledge() {
            transactionsQueue.poll();
            processedEvents.incrementAndGet();
        }

        private double withRounding(double d1, double d2, BiFunction<BigDecimal, BigDecimal, BigDecimal> function) {
            var bd1 = new BigDecimal(d1).setScale(2, RoundingMode.HALF_UP);
            var bd2 = new BigDecimal(d2).setScale(2, RoundingMode.HALF_UP);
            return function.apply(bd1, bd2).doubleValue();
        }

        public long getProcessedEvents() {
            return processedEvents.get();
        }
    }
}

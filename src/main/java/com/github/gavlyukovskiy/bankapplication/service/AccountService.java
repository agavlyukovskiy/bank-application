package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.dao.AccountDao;
import com.github.gavlyukovskiy.bankapplication.domain.Account;
import com.github.gavlyukovskiy.bankapplication.domain.web.CreateAccountRequest;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static com.github.gavlyukovskiy.bankapplication.validation.GenericValidator.validate;

public class AccountService {

    private static final Logger log = LoggerFactory.getLogger(AccountService.class);

    private final Jdbi jdbi;

    public AccountService(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    public Account createAccount(CreateAccountRequest createAccountRequest) {
        validateAccount(createAccountRequest);

        var account = new Account();
        account.setAccountId(UUID.randomUUID().toString());
        account.setAccountName(createAccountRequest.getAccountName());
        account.setCurrency(createAccountRequest.getCurrency());
        account.setBalance(0.);
        account.setCreatedAt(LocalDateTime.now());

        jdbi.useTransaction(handle -> {
            var accountDao = handle.attach(AccountDao.class);
            accountDao.insert(account);
        });

        log.info("Account [{}, {}] was successfully created", account.getAccountId(), account.getCurrency());

        return account;
    }

    public Optional<Account> getAccount(String accountId) {
        return jdbi.inTransaction(handle -> {
            var accountDao = handle.attach(AccountDao.class);
            return accountDao.getAccount(accountId);
        });
    }

    private void validateAccount(CreateAccountRequest createAccountRequest) {
        validate(createAccountRequest.getCurrency() != null, "Currency must not be null");
        validate(createAccountRequest.getAccountName() != null && createAccountRequest.getAccountName().length() < 50, "Account name must be within 50 symbols");
    }
}

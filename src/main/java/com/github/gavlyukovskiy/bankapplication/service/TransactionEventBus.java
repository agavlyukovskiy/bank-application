package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.domain.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class TransactionEventBus {

    private static final Logger log = LoggerFactory.getLogger(TransactionEventBus.class);

    private final List<Consumer<Transaction>> listeners = new CopyOnWriteArrayList<>();
    private final AtomicBoolean shutdown = new AtomicBoolean(false);

    public void addListener(Consumer<Transaction> listener) {
        listeners.add(listener);
    }

    public void transactionStatusChange(Transaction transaction) {
        if (shutdown.get()) {
            throw new IllegalStateException("Event bus is shutting down");
        }
        log.info("Transaction [{}] status was changed to [{}]", transaction.getTransactionId(), transaction.getStatus());
        listeners.forEach(listener -> listener.accept(transaction));
    }

    public void shutdown() {
        shutdown.set(true);
    }
}

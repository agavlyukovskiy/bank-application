package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.domain.Account;
import com.github.gavlyukovskiy.bankapplication.domain.Transaction;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionStatus;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Transaction processor designed to be replaceable by standalone application for high scalability.
 *
 * Worker thread (potentially standalone application) reads transactions from queue that is
 * partitioned by {@link Account#getAccountId()} (potentially partitioned Kafka topics).
 * Partitioning guarantees that same account will be processed by the same worker node/thread thus
 * we don't need any locking on database side.
 *
 * Transaction processing consists of two separate stages:
 * - withdrawal - processing transactions in state {@link TransactionStatus#SUBMITTED} partitioned by {@link Transaction#getSenderId()}
 *    when sender account balance is validated and decreased on the transaction amount, then both account with new balance
 *    and transaction with status {@link TransactionStatus#TRANSIT} are updated atomically and event of changing transaction status
 *    submitted into the queue partitioned by {@link Transaction#getReceiverId()}, if both operations are successful
 *    message is removed (acknowledged) from the queue.
 * - depositing - processing transactions in state {@link TransactionStatus#TRANSIT} partitioned by {@link Transaction#getReceiverId()}
 *    receiver's balance is increased to the transaction amount, then both account with new balance
 *    and transaction with status {@link TransactionStatus#COMPLETED} are updated atomically.
 *
 * This process guarantees to be exactly-once because of atomic update of the account and transaction state - if event state
 * contradicts with actual transaction state it means that processing has been failed between atomic update and message acknowledge.
 */
public class TransactionProcessingService {
    private static final Logger log = LoggerFactory.getLogger(TransactionProcessingService.class);

    private final List<TransactionProcessor> transactionProcessors;

    public TransactionProcessingService(
            Properties properties,
            Jdbi jdbi,
            AccountService accountService,
            TransactionService transactionService,
            TransactionEventBus transactionEventBus
    ) {
        var submittedWorkers = Integer.parseInt(properties.getProperty("transaction-processing.workers-count"));
        var submittedQueueSize = Integer.parseInt(properties.getProperty("transaction-processing.queue-size"));

        this.transactionProcessors = new ArrayList<>(submittedWorkers);
        for (int i = 0; i < submittedWorkers; i++) {
            transactionProcessors.add(new TransactionProcessor(i, submittedQueueSize, jdbi, transactionEventBus, accountService, transactionService));
        }

        transactionEventBus.addListener(transaction -> {
             if (transaction.getStatus() == TransactionStatus.SUBMITTED) {
                partitionToQueue(transaction.getSenderId(), transaction);
            } else if (transaction.getStatus() == TransactionStatus.TRANSIT) {
                partitionToQueue(transaction.getReceiverId(), transaction);
            }
        });
    }

    /**
     * Waits up to 10s to finish all workers, all worker queues must be empty for graceful shutdown.
     */
    public void shutdown() {
        try {
            var started = System.currentTimeMillis();
            log.info("Waiting up to 10s to shutdown all processors");
            while (!transactionProcessors.stream().allMatch(TransactionProcessor::isEmpty)
                    && System.currentTimeMillis() - started < 10000) {
                try {
                    log.info("Some processors are still processing events, waiting 100ms to check again");
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new IllegalStateException("Interrupted, data may be lost");
                }
            }
        } finally {
            transactionProcessors.forEach(TransactionProcessor::shutdown);
        }
    }

    public List<TransactionProcessor> getTransactionProcessors() {
        return transactionProcessors;
    }

    private void partitionToQueue(String partitionKey, Transaction transaction) {
        var hash = partitionKey.hashCode();
        var partition = Math.abs(hash) % transactionProcessors.size();
        transactionProcessors.get(partition).offer(transaction);
    }
}

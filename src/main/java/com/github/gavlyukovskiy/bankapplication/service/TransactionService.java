package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.Constants;
import com.github.gavlyukovskiy.bankapplication.dao.TransactionDao;
import com.github.gavlyukovskiy.bankapplication.domain.web.DepositRequest;
import com.github.gavlyukovskiy.bankapplication.domain.web.SubmitTransactionRequest;
import com.github.gavlyukovskiy.bankapplication.domain.Transaction;
import com.github.gavlyukovskiy.bankapplication.domain.TransactionStatus;
import org.jdbi.v3.core.Jdbi;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static com.github.gavlyukovskiy.bankapplication.validation.GenericValidator.validate;

public class TransactionService {

    private final Jdbi jdbi;
    private final AccountService accountService;
    private final TransactionEventBus transactionEventBus;

    public TransactionService(Jdbi jdbi, AccountService accountService, TransactionEventBus transactionEventBus) {
        this.jdbi = jdbi;
        this.accountService = accountService;
        this.transactionEventBus = transactionEventBus;
    }

    /**
     * Submits transaction of money transfer.
     *
     * @param submitTransactionRequest transaction request
     * @param serviceTransaction service transaction specifies that money transfer is not coming from real account,
     *   such transactions are handled using service account and don't have SUBMITTED phase as there is no money withdrawal.
     * @return transaction state
     */
    public Transaction submitTransaction(SubmitTransactionRequest submitTransactionRequest, boolean serviceTransaction) {
        validateSubmitTransactionRequest(submitTransactionRequest, serviceTransaction);

        var transaction = new Transaction();
        transaction.setTransactionId(UUID.randomUUID().toString());
        if (serviceTransaction) {
            transaction.setSenderId(Constants.SERVICE_ACCOUNT_ID);
            transaction.setStatus(TransactionStatus.TRANSIT);
        } else {
            transaction.setSenderId(submitTransactionRequest.getSenderId());
            transaction.setStatus(TransactionStatus.SUBMITTED);
        }
        transaction.setReceiverId(submitTransactionRequest.getReceiverId());
        transaction.setAmount(submitTransactionRequest.getAmount());
        transaction.setCreatedAt(LocalDateTime.now());
        transaction.setDescription(submitTransactionRequest.getDescription());

        jdbi.useTransaction(handle -> {
            var transactionDao = handle.attach(TransactionDao.class);
            transactionDao.insert(transaction);
        });

        transactionEventBus.transactionStatusChange(transaction);

        return transaction;
    }

    /**
     * Returns current state of transaction.
     */
    public Optional<Transaction> getTransaction(String transactionId) {
        return jdbi.inTransaction(handle -> {
            var transactionDao = handle.attach(TransactionDao.class);
            return transactionDao.getTransaction(transactionId);
        });
    }

    /**
     * Makes deposit to an account from service account.
     */
    public Transaction makeDeposit(DepositRequest depositRequest) {
        var submitTransactionRequest = new SubmitTransactionRequest();
        submitTransactionRequest.setReceiverId(depositRequest.getAccountId());
        submitTransactionRequest.setAmount(depositRequest.getAmount());
        submitTransactionRequest.setDescription("Internal deposit operation: " + depositRequest.getReason());
        return submitTransaction(submitTransactionRequest, true);
    }

    private void validateSubmitTransactionRequest(SubmitTransactionRequest submitTransactionRequest, boolean serviceTransaction) {
        validate(submitTransactionRequest.getAmount() > 0
                && new BigDecimal(submitTransactionRequest.getAmount() * 100).scale() <= 2, "Invalid amount");
        var receiverAccount = accountService.getAccount(submitTransactionRequest.getReceiverId());
        validate(receiverAccount.isPresent(), "Account [" + submitTransactionRequest.getReceiverId() + "] does not exists");
        validate(submitTransactionRequest.getDescription() == null
                || submitTransactionRequest.getDescription().length() <= 200, "Description must be within 200 symbols");
        if (!serviceTransaction) {
            validate(!submitTransactionRequest.getSenderId().equals(submitTransactionRequest.getReceiverId()), "Cannot transfer money to the same account");
            var senderAccount = accountService.getAccount(submitTransactionRequest.getSenderId());
            validate(senderAccount.isPresent(), "Account [" + submitTransactionRequest.getSenderId() + "] does not exists");
            validate(senderAccount.get().getCurrency() == receiverAccount.get().getCurrency(),
                    "Transfer between accounts with different currencies is not supported");
            validate(senderAccount.get().getBalance() >= submitTransactionRequest.getAmount(), "Insufficient funds");
        }
    }
}

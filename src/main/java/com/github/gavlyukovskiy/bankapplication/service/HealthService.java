package com.github.gavlyukovskiy.bankapplication.service;

import com.github.gavlyukovskiy.bankapplication.domain.HealthCheck;
import org.jdbi.v3.core.Jdbi;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;

public class HealthService {
    private final Map<String, Supplier<HealthCheck>> healthChecks = new LinkedHashMap<>();

    public HealthService(Jdbi jdbi, TransactionProcessingService transactionProcessingService) {
        healthChecks.put("db", () -> {
            try {
                jdbi.withHandle(h -> h.select("select 1"));
                return new HealthCheck(true, null);
            } catch (RuntimeException e) {
                return new HealthCheck(false, Map.of("error", e.getMessage()));
            }
        });
        healthChecks.put("transactionProcessing", () -> {
            Map<String, Object> transactionProcessorsHealth = new LinkedHashMap<>();
            var transactionProcessors = transactionProcessingService.getTransactionProcessors();
            boolean allAlive = transactionProcessors.stream().allMatch(TransactionProcessor::isAlive);
            transactionProcessors.forEach(transactionProcessor -> {
                Map<String, Object> transactionProcessorInfo = new LinkedHashMap<>();
                transactionProcessorInfo.put("alive", transactionProcessor.isAlive());
                transactionProcessorInfo.put("queueSize", transactionProcessor.getQueueSize());
                transactionProcessorInfo.put("processedEvents", transactionProcessor.getProcessedEvents());

                transactionProcessorsHealth.put(transactionProcessor.getId(), transactionProcessorInfo);
            });
            return new HealthCheck(allAlive, transactionProcessorsHealth);
        });
    }

    public Map<String, HealthCheck> healthCheck() {
        Map<String, HealthCheck> health = new LinkedHashMap<>();
        healthChecks.forEach((serviceName, healthCheckSupplier) -> health.put(serviceName, healthCheckSupplier.get()));
        return health;
    }
}

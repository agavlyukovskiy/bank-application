create table if not exists `accounts`
(
    accountId varchar(36) not null,
    accountName varchar(50) not null,
    balance double not null,
    currency varchar not null,
    createdAt timestamp not null,
    constraint accounts_pk primary key (accountId)
);

create table if not exists `transactions`
(
    transactionId varchar(36) not null,
    senderId varchar(36) not null,
    receiverId varchar(36) not null,
    amount double not null,
    description varchar(200),
    status varchar not null,
    denyReason varchar(200),
    createdAt timestamp not null,
    processedAt timestamp,
    constraint transactions_pk primary key (transactionId)
);

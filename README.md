## Bank Application

Bank application for money transferring. 

### Requirements
- Java 11

### Build

```bash
./gradlew clean build
```

### Run

```bash
java -jar ./build/libs/bank-application.jar
``` 

### Description
Application exposes User API on port 8080 and Secure (admin) API on port 8081. See API Reference for details. 

Main idea behind implementation is scalable lock-free approach. Current design consists of 
web api components that saves transaction into database and pushes an event for processing 
and transaction processors, that process events.

Worker thread (potentially standalone application) reads transactions from queue that is 
partitioned by AccountId (potentially partitioned Kafka topics).

Partitioning operations related to the same account guarantees that this account will be 
processed by the same worker thread/application thus we don't need any locking on database side.

Transaction processing consists of two separate stages:

- _withdrawal_ - processing transactions in state SUBMITTED partitioned by sender AccountId when 
   sender account balance is validated and decreased on the transaction amount, then both account 
   with new balance and transaction with status TRANSIT are updated atomically and event of changing 
   transaction status submitted into the queue partitioned by receiver AccountId, if both operations
   are successful message is removed (acknowledged) from the queue.  

- _depositing_ - processing transactions in state TRANSIT receiver's balance is increased 
   to the transaction amount, then both account with new balance and transaction with status
   COMPLETED are updated atomically.

This process guarantees to be exactly-once because issues during atomic updates will be reprocessed. 
But if update was already committed and there was an issue during sending event or acknowledgement 
the message won't be acknowledged and on the second retry actual transaction status will 
block repeated change of account's balance.

#### Diagram of the process
![Diagram](diag.png)

### Alternative 
Alternatively transaction workers could try to lock on both accounts in the database and do operation
in place. 

While it's good that both accounts and transaction can be updated at once I find this approach bad 
because if lock acquiring fails transaction either needs to be resubmitted to the queue (essentially 
breaking transactions order) or worker should hold transaction waiting for unlock (which could also 
lead to breaking transactions order). Also in this case we need to develop system to break stale 
locks, which would be additional effort.

### API Reference
#### User Api
##### Create an account

```text
POST /api/accounts
```
```json
{
    "accountName": "test",
    "currency": "USD"
}
```
Response
```json
{
    "accountId": "e8cd9f50-c105-4085-9d0c-235a08b81c5f",
    "accountName": "test",
    "balance": 0.0,
    "currency": "USD",
    "createdAt": "2019-10-14T23:34:42.0706073"
}
```
##### Get account by id

```text
GET /api/accounts/:accountId
```
Response
```json
{
    "accountId": "e8cd9f50-c105-4085-9d0c-235a08b81c5f",
    "accountName": "test",
    "balance": 0.0,
    "currency": "USD",
    "createdAt": "2019-10-14T23:34:42.0706073"
}
```
##### Submit transaction

```text
POST /api/transactions
```
```json
{
    "senderId": "e8cd9f50-c105-4085-9d0c-235a08b81c5f",
    "receiverId": "710da650-2f4c-43ef-a703-436fa0461efb",
    "amount": 42.12,
    "description": "My transaction"
}
```
Response
```json
{
    "transactionId": "c90122ec-a72c-4338-a5e2-3cd5f75fd8c6",
    "senderId": "e8cd9f50-c105-4085-9d0c-235a08b81c5f",
    "receiverId": "710da650-2f4c-43ef-a703-436fa0461efb",
    "amount": 42.12,
    "description": "My transaction",
    "status": "SUBMITTED",
    "createdAt": "2019-10-14T23:44:50.2333555",
    "processedAt": null
}
```
##### Get status of transaction

```text
GET /api/transactions/:transactionId
```
Response
```json
{
    "transactionId": "c90122ec-a72c-4338-a5e2-3cd5f75fd8c6",
    "senderId": "e8cd9f50-c105-4085-9d0c-235a08b81c5f",
    "receiverId": "710da650-2f4c-43ef-a703-436fa0461efb",
    "amount": 42.12,
    "description": "My transaction",
    "status": "COMPLETED",
    "createdAt": "2019-10-14T23:44:50.233356",
    "processedAt": "2019-10-14T23:44:50.287353"
}
```
#### Secure API:
##### Make deposit to an account

```text
POST /secure/transactions/deposit
```
```json
{
    "accountId": "e8cd9f50-c105-4085-9d0c-235a08b81c5f",
    "amount": 42.12,
    "reason": "My transaction"
}
```
Response
```json
{
    "transactionId": "e390fa2d-60cc-4047-b53f-f683f55e615d",
    "senderId": "00000000-0000-0000-0000-000000000000",
    "receiverId": "e8cd9f50-c105-4085-9d0c-235a08b81c5f",
    "amount": 42.12,
    "description": "Internal deposit operation: My transaction",
    "status": "TRANSIT",
    "createdAt": "2019-10-14T23:43:52.5420538",
    "processedAt": null
}
```
##### Service health

```text
GET /secure/health
```
Response
```json
{
    "db": {
        "healthy": true
    },
    "transactionProcessing": {
        "healthy": true,
        "info": {
            "tx-worker-0": {
                "alive": true,
                "queueSize": 0,
                "processedEvents": 3
            }
        }
    }
}
```